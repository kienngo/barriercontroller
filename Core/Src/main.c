/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"
#include "Console.h"
#include "readerComms.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TIME_WARNING_LIGHT 5
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
volatile uint8_t check_limitnow = GPIO_PIN_SET;
volatile uint8_t check_limitdown = GPIO_PIN_SET;
uint8_t checkPeriod = 2;  //2s
uint8_t period_check = false;

uint32_t debouncer_tick_en = 0, debouncer_tick_en_sensor = 0;
uint32_t tickstart = 0, tickstart1 = 0;
uint8_t debouncer_timer = 1, debouncer_sen = 1;


uint8_t g_ligth_counter = 2; //Counter for light timer
uint8_t g_ligth_timer = 0; //flag for light timer


uint8_t g_timer_counter = 6; // Counter for close gate
//uint8_t check_pin_limitup = true;
uint8_t g_timeout_is_en = 0;
extern volatile uint8_t g_status_limit_up, g_status_limit_down;

barrier_timer_t barrier_timer;

//main PV
volatile uint8_t uPin5 = 0, uPin6 = 0, uPin7 = 0, uPin = 0;
volatile uint16_t g_hardware_timeout = 6;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void vGateInit(void);
void vDebouncer(void);
void vDebouncerSensor(void);
void vCheckPeriod(void);
void vGateEventUp(void);
//void ligth_on(void);
//void ligth_off(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  MX_TIM4_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
//  if(HAL_UART_Receive_IT(&huart3, &uReceive3, 1) != HAL_OK){
//  	  Error_Handler();
//  }
  if(HAL_UART_Receive_IT(&huart2, &uReceive2, 1) != HAL_OK){
  	  Error_Handler();
  }
  if(HAL_UART_Receive_IT(&huart1, &uReceive1, 1) != HAL_OK){
  	  Error_Handler();
  }
  HAL_TIM_Base_Start_IT(&htim2);
  HAL_GPIO_WritePin(GPIOC, LED_PIN, LED_OFF);
  ligth_off();
  vGateInit();

  vQueueInitReader();

  __HAL_TIM_CLEAR_FLAG(&htim4, TIM_SR_UIF);
  __HAL_TIM_CLEAR_FLAG(&htim2, TIM_SR_UIF);
  __HAL_TIM_CLEAR_FLAG(&htim3, TIM_SR_UIF);




  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
//	uplink_cmd_handler();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
//	vDetectLimitPin();
	vGetTimeout();
//	debouncer_timer2 = g_hardware_timeout + 5;
	vQueueReader1();
	vQueueReader2();

	//Debouncer when interrupt
	vDebouncer();
	vDebouncerSensor();
	//


	//Timer3 TimerOut -> Close()
	vControlTimeOut();

//	vHardCheckLimitUp();

	vCheckPeriod();

//	if(g_hardware_timeout == g_timer_counter) ligth_off();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void vGetTimeout(void){
	uPin5 = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5);
	uPin6 = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6);
	uPin7 = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7);
	uPin = uPin5 | (uPin6 << 1) | (uPin7 << 2);
	switch (uPin) {
		case 0:
			g_hardware_timeout = 6;
			break;
		case 1:
			g_hardware_timeout = 9;
			break;
		case 2:
			g_hardware_timeout = 12;
			break;
		case 3:
			g_hardware_timeout = 15;
			break;
		case 4:
			g_hardware_timeout = 18;
			break;
		case 5:
			g_hardware_timeout = 21;
			break;
		case 6:
			g_hardware_timeout = 360;
			break;
		case 7:
			g_hardware_timeout = 36000;
			break;
		default:
			g_hardware_timeout = 15;
			break;
	}
//	g_timer_counter = g_hardware_timeout;
}

void ligth_off(void){
	HAL_GPIO_WritePin(GPIOB, LIGTH_PIN, LIGHT_DIS);
}
void ligth_on(void){
	HAL_GPIO_WritePin(GPIOB, LIGTH_PIN, LIGHT_EN);
}
void vGateInit(void){
	HAL_GPIO_WritePin(GPIOB, GATE_UP_PIN,    GATE_INIT);
	HAL_GPIO_WritePin(GPIOB, GATE_DOWN_PIN,  GATE_INIT);

}

/*
 * @Description: TIM4 PeriodElapsedCallback
 * @Timer: TIM4, TIM2
 * @Timeout: TIM4: 1s, TIM2 1s
 *
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance == TIM4){ //Timer for gate
		HAL_GPIO_TogglePin(GPIOC, LED_PIN);
		g_timer_counter--;
		if(g_timer_counter <= TIME_WARNING_LIGHT){
			ligth_on();
		}
		if(g_timer_counter == 0){
			g_timeout_is_en = 1;

			g_timer_counter = g_hardware_timeout;
			HAL_TIM_Base_Stop_IT(&htim4);

//			check_pin_limitup = true;
//			HAL_TIM_Base_Start_IT(&htim2);

		}
	}
	if(htim->Instance == TIM2){
//		HAL_GPIO_TogglePin(GPIOC, LED_PIN);
		checkPeriod--;
		if(checkPeriod == 0){
			HAL_TIM_Base_Stop_IT(&htim2);

			checkPeriod = 2;
			period_check = true;
		}


	}
	if(htim->Instance == TIM3){
		g_ligth_counter--;
		if(g_ligth_counter == 0){

			g_ligth_counter = 2;
			g_ligth_timer = 0;
			HAL_TIM_Base_Stop_IT(&htim3);
			ligth_off();
		}

	}
}
void vTimeOutBarrier(void){
	g_timer_counter = g_hardware_timeout;
	while(HAL_TIM_Base_Start_IT(&htim4) != HAL_OK);

}
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if(GPIO_Pin == LIMIT_UP_PIN){
		if(debouncer_timer == 1){
			debouncer_timer = 0; //Enable debounce 1000ms
			HAL_GPIO_WritePin(GPIOC, LED_PIN, LED_OFF);
			g_timer_counter = g_hardware_timeout;


		    //
//			check_pin_limitup = false;

			//
//			ligth_off();
//
//			vTimeOutBarrier();
//			HAL_TIM_Base_Start_IT(&htim2);
		}
	}
	if(GPIO_Pin == LIMIT_DOWN_PIN){

//		HAL_GPIO_WritePin(GPIOC, LED_PIN, LED_OFF);
//		ligth_off();

//		g_timeout_is_en = 0;
		if(debouncer_sen == 1){
			debouncer_sen = 0;
		}
//		g_timer_counter = g_hardware_timeout;
//		ligth_off();
//		if(HAL_GPIO_ReadPin(GPIOB, LIMIT_UP_PIN) == GPIO_PIN_RESET){
//			if(g_timer_counter <= TIME_WARNING_LIGHT) open_gate();
////			vTimeOutBarrier();
//
//		}
//		vTimeOutBarrier();
//		HAL_TIM_Base_Stop_IT(&htim4);


	}

}
void vControlTimeOut(void){

	if(g_timeout_is_en == 1){

		g_timer_counter = g_hardware_timeout;
		HAL_TIM_Base_Stop_IT(&htim4);
		g_timeout_is_en = 0;

		if(g_ligth_timer == 0){
			HAL_TIM_Base_Start_IT(&htim3); //Timer for light
			g_ligth_timer = 1;
		}
	//Controller Barrier
		close_gate();

	}
}

void vCheckPeriod(void){
	if(period_check == true) {

		while(HAL_TIM_Base_Start_IT(&htim2) != HAL_OK);
		period_check = false;
	    check_limitnow = HAL_GPIO_ReadPin(GPIOB,LIMIT_UP_PIN);
		if(check_limitnow == GPIO_PIN_RESET){
			HAL_Delay(200);
			check_limitnow = HAL_GPIO_ReadPin(GPIOB,LIMIT_UP_PIN);
			if(check_limitnow == GPIO_PIN_RESET){
				if(g_hardware_timeout == g_timer_counter){
					vGateEventUp();
				}
			}
		}
	}

}
void vDebouncer(void){
	if(debouncer_timer == 0){
		if(debouncer_tick_en == 0){
			tickstart = HAL_GetTick();
			debouncer_tick_en = 1;
		}
		if ((HAL_GetTick() - tickstart) > 2501) // uint32_t wait = 500 + (uint32_t)(uwTickFreq)  = 1500ms
		{
			debouncer_timer   = 1;
			debouncer_tick_en = 0;

			//2nd check to identified limitUp is enable not noise
			check_limitnow = HAL_GPIO_ReadPin(GPIOB,LIMIT_UP_PIN);
			if(check_limitnow == GPIO_PIN_RESET){
				if(g_timer_counter == g_hardware_timeout){
//					vGateEventUp();==
				}

			}
		}
	}
}
void vDebouncerSensor(void){
	if(debouncer_sen == 0){
		if(debouncer_tick_en_sensor == 0){
			tickstart1 = HAL_GetTick();
			debouncer_tick_en_sensor = 1;
		}
		if ((HAL_GetTick() - tickstart1) > 101) // uint32_t wait = 500 + (uint32_t)(uwTickFreq)  = 1500ms
		{
			debouncer_sen   = 1;
			debouncer_tick_en_sensor = 0;

			//2nd check to identified limitUp is enable not noise
			check_limitdown = HAL_GPIO_ReadPin(GPIOB,LIMIT_DOWN_PIN);
			if(check_limitdown == GPIO_PIN_RESET){
				g_timer_counter = g_hardware_timeout;
				ligth_off();
			}
		}
	}
}

void vGateEventUp(void){
	ligth_off();
	vTimeOutBarrier();
//	HAL_TIM_Base_Start_IT(&htim2);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
