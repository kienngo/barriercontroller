/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */
uint8_t uReceive1;
uint8_t uReceive2;
uint8_t uReceive3;

//Array for RingBuffer Algorithm
usartbuffer_t aBuffer[3];
/* USER CODE END 0 */

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USART1 init function */

void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 38400;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }

}
/* USART2 init function */

void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */
    /* USART1 clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART1 GPIO Configuration    
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspInit 1 */

  /* USER CODE END USART1_MspInit 1 */
  }
  else if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspInit 0 */

  /* USER CODE END USART2_MspInit 0 */
    /* USART2 clock enable */
    __HAL_RCC_USART2_CLK_ENABLE();
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART2 GPIO Configuration    
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART2 interrupt Init */
    HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspInit 1 */

  /* USER CODE END USART2_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspDeInit 0 */

  /* USER CODE END USART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();
  
    /**USART1 GPIO Configuration    
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9|GPIO_PIN_10);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspDeInit 1 */

  /* USER CODE END USART1_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspDeInit 0 */

  /* USER CODE END USART2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART2_CLK_DISABLE();
  
    /**USART2 GPIO Configuration    
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2|GPIO_PIN_3);

    /* USART2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspDeInit 1 */

  /* USER CODE END USART2_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
uint8_t buffer_level(uint8_t uart_mask, uint8_t direction){
	return aBuffer[uart_mask].buffer[direction].write - aBuffer[uart_mask].buffer[direction].read;
}
//Transmit buffer

uint8_t read_buffer8(uint8_t uart_mask, uint8_t direction, uint8_t *data){

	if(aBuffer[uart_mask].buffer[direction].read == aBuffer[uart_mask].buffer[direction].write) return 1;
	*data = aBuffer[uart_mask].buffer[direction].data[ aBuffer[uart_mask].buffer[direction].read];
	 aBuffer[uart_mask].buffer[direction].read = ( aBuffer[uart_mask].buffer[direction].read + 1) & BUFFERMASK;
	return 0;

}
//Receive data
//uint8_t write_buffer8(uint8_t uart_mask, uint8_t direction, uint8_t data){
//	uint8_t next_index;
//
//	next_index = (aBuffer[uart_mask].buffer[direction].write + 1) & BUFFERMASK;
//	while(aBuffer[uart_mask].buffer[direction].read == next_index);
//	aBuffer[uart_mask].buffer[direction].data[aBuffer[uart_mask].buffer[direction].write] = data;
//	aBuffer[uart_mask].buffer[direction].write = next_index;
//
//	/*
//	 * direction == TX : write buffer and Transmit buffer
//	 * direction == RX : Only write buffer
//	 */
//	uint8_t dataS = 0;
//
//	if(direction == TX){
//		int temp = buffer_level(uart_mask, direction);
//		if(temp > 0){
//			if(!(temp & 1)){
//				dataSend[0] = 0;
//				read_buffer8(uart_mask, direction, &dataS);
//				dataSend[0] |= ((dataS & 0x01) << 8);
//				read_buffer8(uart_mask, direction, &dataS);
//				dataSend[0] |= dataS & 0xFF;
//				switch (uart_mask) {
//					case MDB_USART:
//						HAL_UART_Transmit(MACRO_MDB_USART, (uint8_t*)dataSend, 1,HAL_MAX_DELAY);
//						break;
//					case BILL_USART:
//						HAL_UART_Transmit(MACRO_BIL_USART, (uint8_t*)dataSend, 1,HAL_MAX_DELAY);
//						break;
//
//				}
//			}
//		}
//	}
//	return 0;
//}
uint8_t up_write_buffer8(uint8_t uart_mask, uint8_t direction, uint8_t data){
	uint8_t next_index;

	next_index = (aBuffer[uart_mask].buffer[direction].write + 1) & BUFFERMASK;
	while(aBuffer[uart_mask].buffer[direction].read == next_index);
	aBuffer[uart_mask].buffer[direction].data[aBuffer[uart_mask].buffer[direction].write] = data;
	aBuffer[uart_mask].buffer[direction].write = next_index;

	/*
	 * direction == TX : write buffer and Transmit buffer
	 * direction == RX : Only write buffer
	 */
	uint8_t dataS = 0;
	uint16_t u16_dtemp = 0;
	if(direction == TX){
		int temp = buffer_level(uart_mask, direction);
		if(temp > 0){
				read_buffer8(uart_mask, direction, &dataS);
				u16_dtemp = dataS;
				switch(uart_mask){
				case MASK_UART1:
					HAL_UART_Transmit(MACRO_USART1, (uint8_t *)&u16_dtemp, 1,1000);
					break;
				case MASK_UART2:
					HAL_UART_Transmit(MACRO_USART2, (uint8_t *)&u16_dtemp, 1,1000);
					break;
				case MASK_UART3:
//					HAL_UART_Transmit(MACRO_USART3, (uint8_t *)&u16_dtemp, 1,1000);
					break;
				}

		}
	}
	return 0;
}
uint8_t send_char(uint8_t uart_mask, char c){
	return write_buffer8(uart_mask, TX, (uint8_t)c);
}
uint8_t up_send_char(uint8_t uart_mask, char c){
	return up_write_buffer8(uart_mask, TX, (uint8_t)c);
}
uint8_t recv_char(uint8_t uart_mask,uint8_t* c){
	//Blocking
	return read_buffer8(uart_mask, RX, (uint8_t*)c);
}
uint8_t send_str(uint8_t uart_mask, char *str){
    while(*str) {
        // if an error occurs
        if(send_char(uart_mask, *str)) return 1;
        str++;
    }
    return 0;
}
uint8_t up_send_str(uint8_t uart_mask, char *str){
    while(*str) {
        // if an error occurs
        if(up_send_char(uart_mask, *str)) return 1;
        str++;
    }
    return 0;
}
//Read data in the buffer
uint8_t recv_str(uint8_t uart_mask, char *str){
	char c;
	while(1) {
		if(recv_char(uart_mask, (uint8_t*)&c)) return 1;
		*str = c;
		str++;
		if(c == '\0') return 0;
	}
}
void vDisableUart(void){
	HAL_UART_AbortReceive_IT(&huart2);
	HAL_UART_AbortReceive_IT(&huart1);
}
void vRecallUart(void){
	HAL_UART_Receive_IT(MACRO_USART2, &uReceive2, 1);
	HAL_UART_Receive_IT(MACRO_USART1, &uReceive1, 1);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  if(huart->Instance == USART3){
//	  up_write_buffer8(MASK_UART3, RX, uReceive3); // Write lb
//	  HAL_UART_Receive_IT(MACRO_USART3, &uReceive3, 1);
  }
  else if(huart->Instance == USART2)
  {

	  up_write_buffer8(MASK_UART2, RX, uReceive2); // Write lb
	  HAL_UART_Receive_IT(MACRO_USART2, &uReceive2, 1);
  }
  else if(huart->Instance == USART1)
  {
//	  write_buffer8(BILL_USART, RX, uplink_Receive8[1]); // Write hb
	  up_write_buffer8(MASK_UART1, RX, uReceive1); // Write lb
	  HAL_UART_Receive_IT(MACRO_USART1, &uReceive1, 1);
  }

}

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define UART1_LOG_PUTCHAR int __io_putchar(int ch)
#else
#define LCD_LOG_PUTCHAR int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

UART1_LOG_PUTCHAR
{
//	 HAL_UART_Transmit(MACRO_USART3, (uint8_t *)&ch, 1,HAL_MAX_DELAY);
	 return ch;
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
