/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for 
  *                      the gpio  
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */
#define LED_PIN GPIO_PIN_13
#define GATE_UP_PIN    GPIO_PIN_5
#define GATE_DOWN_PIN  GPIO_PIN_4
#define LIMIT_UP_PIN   GPIO_PIN_8
#define LIMIT_DOWN_PIN GPIO_PIN_9
#define LIGTH_PIN	   GPIO_PIN_10
#define LED_OFF   GPIO_PIN_SET
#define LED_ON	  GPIO_PIN_RESET
#define GATE_UP   GPIO_PIN_SET
#define GATE_DOWN GPIO_PIN_SET
#define GATE_INIT GPIO_PIN_RESET
#define GATE_LIMIT_INIT GPIO_PIN_SET
#define LIGHT_EN  GPIO_PIN_SET
#define LIGHT_DIS GPIO_PIN_RESET
/* USER CODE END Private defines */

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
