/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */
 /* Activate a USART buffer for appropriate USART */
 #define MASK_UART3        2
 #define MASK_UART2        1
 #define MASK_UART1        0
 #define MACRO_USART1  &huart1
 #define MACRO_USARTD  &huart1
// #define MACRO_USART3  &huart3
 #define MACRO_USART2  &huart2
  //#define UPLINK_USART     2
 /* set the buffersize for all USART buffers */
 #define BUFFERSIZE       128
 #define BUFFERMASK       (BUFFERSIZE - 1);

  /* Don't change the following two defines */
 #define RX              0
 #define TX              1

 typedef struct _buffer {
 	uint8_t data[BUFFERSIZE];
 	volatile uint8_t read;
 	volatile uint8_t write;
 } buffer_t;
 typedef struct usartbuffer {
     buffer_t buffer[2];
 } usartbuffer_t;

/* USER CODE END Includes */

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

/* USER CODE BEGIN Private defines */
extern uint8_t uReceive1;
extern uint8_t uReceive2;
extern uint8_t uReceive3;
/* USER CODE END Private defines */

void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);

/* USER CODE BEGIN Prototypes */
/* Callback
 *        */
void vDisableUart(void);
void vRecallUart(void);
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);

uint8_t buffer_level(uint8_t uart_mask, uint8_t direction);
uint8_t read_buffer8(uint8_t uart_mask, uint8_t direction, uint8_t *data);
uint8_t write_buffer8(uint8_t uart_mask, uint8_t direction, uint8_t data);
uint8_t up_write_buffer8(uint8_t uart_mask, uint8_t direction, uint8_t data);
uint8_t send_char(uint8_t uart_mask, char c);
uint8_t up_send_char(uint8_t uart_mask, char c);
uint8_t recv_char(uint8_t uart_mask,uint8_t* c);
uint8_t send_str(uint8_t uart_mask, char *str);
uint8_t up_send_str(uint8_t uart_mask, char *str);
uint8_t recv_str(uint8_t uart_mask, char *str);
uint8_t send_mdb(uint8_t uart_mask, uint16_t mdb);
uint16_t recv_mdb(uint8_t uart_mask);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
