/*
 * Console.h
 *
 *  Created on: Feb 12, 2020
 *      Author: Zeder
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_

#define MAX_CMD_LENGTH      20
#define MAX_VAR             10



typedef struct {
    char* cmd;
    void(*funcptr)(char *arg);
} cmdStruct_t;


void uplink_cmd_handler(void);
void parse_cmd(char *cmd);
void cmd_help(char *arg);
void cmd_reset(char *arg);
void cmd_timeout(char *arg);
void cmd_timeout_info(char *arg);
void cmd_config_timeout(char*arg);

#endif /* CONSOLE_H_ */
