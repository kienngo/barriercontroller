/*
 * Console.c
 *
 *  Created on: Feb 12, 2020
 *      Author: Zeder
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "usart.h"
#include "Console.h"

extern volatile int uSTimeOut;
extern  uint8_t uHTimeOut;
uint8_t enSoftTimeOut = 0;
cmdStruct_t CMD_LIST[] = {
    {"reset",cmd_reset},
    {"help",cmd_help},
	{"en-timeout",cmd_config_timeout},
	{"timeout",cmd_timeout},
	{"timeout-info",cmd_timeout_info},
	{NULL,NULL}
};


char buff[50];

void uplink_cmd_handler(void){
	static uint8_t cmd[20];
	static uint8_t index = 0;

	if(buffer_level(MASK_UART3, RX) < 1) return;
	// flush cmd buffer if cmd is out of a valid length
	if(index == MAX_CMD_LENGTH) {
		index = 0;
	}

	// append char to cmd
	//    cdc_recv_char(&cmd[index]);
	recv_char(MASK_UART3, &cmd[index]);

	switch(cmd[index]) {
	case '\r':
		// carriage return received, replace with stringtermination and parse
		//send_str(UPLINK_USART, "\r\n");
		cmd[index] = '\0';
		parse_cmd((char*)cmd);
		index = 0;
		break;
	case '\n':
		// do nothing, but avoid index from incrementing
		break;
	case '\b':
		// backspace, remove last received char
		index--;
		up_send_char(MASK_UART3, '\b');
		break;
		// char is part of an ESC sequence
	case 0x1B:
	case 0x5B:
		index++;
		break;
		// each other if the last two char was not part of an ESC sequence
	default:
		if(cmd[index - 1] == 0x5B && cmd[index - 2] == 0x1B) {
			index = index - 2;
		} else {
			//send_char(UPLINK_USART, cmd[index]);
			index++;
		}
	}
}
void parse_cmd(char *cmd){
	char *tmp;
	uint8_t index = 0;

    // seperate command from arguments
    tmp = strsep(&cmd," ");

    // search in command list for the command
	while(strcasecmp(CMD_LIST[index].cmd,tmp)) {
        if(CMD_LIST[index + 1].cmd == NULL) {
//            cdc_send_str((char*)("Error: Unknown command\r\n"));
        	up_send_str(MASK_UART3, (char*)("Error: Unknown command\r\n"));
            return;
        }
        index++;
    }

    // run the command
    CMD_LIST[index].funcptr(cmd);
	return;
}
void cmd_reset(char *arg){
	NVIC_SystemReset();
}
void cmd_help(char *arg){
	up_send_str(MASK_UART3,(char*)("-----------------------------------------------\r\n"));
	up_send_str(MASK_UART3,(char*)("reset:\r\n   reset the IO-Board\r\n"));
	up_send_str(MASK_UART3,(char*)("info:\r\n    ------------------------------------\r\n"));
	up_send_str(MASK_UART3,(char*)("timeout <time out>:\r\n   set time out barrirer <time out>(s) example: timeout 10\r\n"));
	up_send_str(MASK_UART3,(char*)("en-timeout <1/0>:\r\n   enable/disable soft timeout example: en-timeout 1\r\n"));
	up_send_str(MASK_UART3,(char*)("timeout-info:\r\n   get current timeout type\r\n"));
	up_send_str(MASK_UART3,(char*)("destroy:    to destroy the barrier\r\n"));
	up_send_str(MASK_UART3,(char*)("-----------------------------------------------\r\n"));
}
void cmd_timeout(char *arg){
	if(enSoftTimeOut){
		uSTimeOut = atoi(arg)*1000;
		sprintf(buff,"\r\n  Soft TimeOut:%ds \r\n",uSTimeOut/1000);
		up_send_str(MASK_UART3,(char*)buff);
	}
	else{
		up_send_str(MASK_UART3,(char*)("\r\n Please use dipswitch\r\n"));
	}


}
void cmd_timeout_info(char *arg){
	if(enSoftTimeOut){
		sprintf((buff),"\r\n  Soft TimeOut: %ds \r\n ",uSTimeOut/1000);
		up_send_str(MASK_UART3,(char*)buff);
	}
	else{
		sprintf((buff),"\r\n  Hard TimeOut: %ds \r\n ",uHTimeOut/1000);
		up_send_str(MASK_UART3,(char*)buff);
	}
}
void cmd_config_timeout(char*arg){
	enSoftTimeOut = atoi(arg);
	if(enSoftTimeOut){
		up_send_str(MASK_UART3,(char*)"\r\n TimeOut set by Software \r\n ");
	}
	else{
		up_send_str(MASK_UART3,(char*)"\r\n  TimeOut set by Harware \r\n ");
	}
}
