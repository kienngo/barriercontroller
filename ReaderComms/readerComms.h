/*
 * readerComms.h
 *
 *  Created on: Feb 12, 2020
 *      Author: Zeder
 */

#ifndef READERCOMMS_H_
#define READERCOMMS_H_



void vQueueInitReader(void);

void vLimitFlagInit(void);
void vDetectLimitPin(void);

void vQueueReader1(void);
void vQueueReader2(void);

void command(char *cmd,uint8_t MASK);
void cmd_open_gate(char *arg);

void open_gate(void);
void close_gate(void);

#endif /* READERCOMMS_H_ */
