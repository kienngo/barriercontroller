/*
 * readerComms.c
 *
 *  Created on: Feb 12, 2020
 *      Author: Zeder
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "Console.h"
#include "readerComms.h"
#include "gpio.h"
cmdStruct_t CTL_LIST[] = {
    {"OP",cmd_open_gate},
	{NULL,NULL}
};
extern uint8_t g_timer_counter;
extern uint8_t g_timeout_is_en;

uint8_t CURRENT_MASK = MASK_UART1;
extern volatile uint16_t g_hardware_timeout;
volatile uint8_t g_status_limit_up = GPIO_PIN_SET, g_status_limit_down = GPIO_PIN_SET;




volatile int uSTimeOut = 1*1000;
extern uint8_t enSoftTimeOut;

void vQueueInitReader(void){
	g_timeout_is_en = 0;
	vLimitFlagInit();
}
void vLimitFlagInit(void){
	g_status_limit_up = g_status_limit_down = GPIO_PIN_SET;
}
void vQueueReader1(void){
	static uint8_t cmd[20];
	static uint8_t index = 0;

	if(buffer_level(MASK_UART1, RX) < 1) return;
	// flush cmd buffer if cmd is out of a valid length
	if(index == MAX_CMD_LENGTH) {
		index = 0;
	}

	// append char to cmd
	//    cdc_recv_char(&cmd[index]);
	recv_char(MASK_UART1, &cmd[index]);

	switch(cmd[index]) {
	case '\r':
		// carriage return received, replace with stringtermination and parse
		//send_str(UPLINK_USART, "\r\n");
		cmd[index] = '\0';
		command((char*)cmd, MASK_UART1);
		index = 0;
		break;
	case '\n':
		// do nothing, but avoid index from incrementing
		break;
	case '\b':
		// backspace, remove last received char
		index--;
		up_send_char(MASK_UART1, '\b');
		break;
		// char is part of an ESC sequence
	case 0x1B:
	case 0x5B:
		index++;
		break;
		// each other if the last two char was not part of an ESC sequence
	default:
		if(cmd[index - 1] == 0x5B && cmd[index - 2] == 0x1B) {
			index = index - 2;
		} else {
			//send_char(UPLINK_USART, cmd[index]);
			index++;
		}
	}
}
void vQueueReader2(void){
	static uint8_t cmd[20];
	static uint8_t index = 0;

	if(buffer_level(MASK_UART2, RX) < 1) return;
	// flush cmd buffer if cmd is out of a valid length
	if(index == MAX_CMD_LENGTH) {
		index = 0;
	}

	// append char to cmd
	//    cdc_recv_char(&cmd[index]);
	recv_char(MASK_UART2, &cmd[index]);

	switch(cmd[index]) {
	case '\r':
		// carriage return received, replace with stringtermination and parse
		//send_str(UPLINK_USART, "\r\n");
		cmd[index] = '\0';
		command((char*)cmd, MASK_UART2);
		index = 0;
		break;
	case '\n':
		// do nothing, but avoid index from incrementing
		break;
	case '\b':
		// backspace, remove last received char
		index--;
		up_send_char(MASK_UART2, '\b');
		break;
		// char is part of an ESC sequence
	case 0x1B:
	case 0x5B:
		index++;
		break;
		// each other if the last two char was not part of an ESC sequence
	default:
		if(cmd[index - 1] == 0x5B && cmd[index - 2] == 0x1B) {
			index = index - 2;
		} else {
			//send_char(UPLINK_USART, cmd[index]);
			index++;
		}
	}
}

void command(char *cmd,uint8_t MASK){
	char *tmp;
	uint8_t index = 0;
	CURRENT_MASK = MASK;
    // seperate command from arguments
    tmp = strsep(&cmd," ");

    // search in command list for the command
	while(strcasecmp(CTL_LIST[index].cmd,tmp)) {
        if(CTL_LIST[index + 1].cmd == NULL) {
//            cdc_send_str((char*)("Error: Unknown command\r\n"));
        	up_send_str(MASK, (char*)("Error: Unknown command\r\n"));
            return;
        }
        index++;
    }

    // run the command
	CTL_LIST[index].funcptr(cmd);

	return;
}

void control_gate(void){

//	open_gate();
//	close_gate();
}
void close_gate(){


//	HAL_GPIO_WritePin(GPIOC, LED_PIN, LED_OFF);


	HAL_GPIO_WritePin(GPIOB, GATE_DOWN_PIN, GATE_DOWN);
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOB, GATE_DOWN_PIN, GATE_INIT);
//	vRecallUart();
}
void open_gate(){
	//Remeber flag for this event
	g_timer_counter = g_hardware_timeout;


	HAL_GPIO_WritePin(GPIOB, GATE_UP_PIN, GATE_UP);
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOB, GATE_UP_PIN, GATE_INIT);


}

void vDetectLimitPin(void){
//	uStatusLimitUp = HAL_GPIO_ReadPin(GPIOB, LIMIT_UP_PIN);
//	uStatusLimitDown = HAL_GPIO_ReadPin(GPIOB, LIMIT_DOWN_PIN);
}


void cmd_open_gate(char *arg){
	if(HAL_GPIO_ReadPin(GPIOB, LIMIT_UP_PIN) == GPIO_PIN_RESET){
		ligth_off();
	}
	else ligth_on();

	open_gate();
}
